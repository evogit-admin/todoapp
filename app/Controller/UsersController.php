<?php
/**
 * Controlador de usuarios del sistema TodoApp
 *
 * Encargado de la lógica de negocio sobre los usuarios del sistema
 *
 * @author Ing. Luis Gabriel Rodriguez <luisgabriel84@gmail.com>
 **/
App::uses('AppController', 'Controller');

class UsersController extends AppController {

	/**
	 * Listado de componentes
	 */
	public $components = array(
        'Paginator' => array(
            'User' => array(
				'order' => array(
					'User.created' => 'desc'
				)
            )
        )
    );

	/**
	 * Ejecución de acciones previo a la carga
	 */
	public function beforeFilter(){
		parent::beforeFilter();


	}


	/**
	 * Login de usuarios
	 */
    public function login() {
		$this->layout ='login';

		//Si el usuario ya inicio sessión redirigirlo
		if($this->Session->check('Auth.User')){
			$user = $this->Session->read('Auth.User');
			if($user['access_level']==1){
					return $this->redirect('/backend/users');
			}
			return $this->redirect('/tasks');
		}

		// Autenticar el usuario
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {

				$user = $this->Session->read('Auth.User');
				if($user['access_level']==1){
					return $this->redirect('/backend/users');
				}
				return $this->redirect('/tasks');

			}
			$this->Session->setFlash('Usuario o clave incorrectos');
		}
	}

	/**
	 * Login de administrador
	 */
	public function backend_login() {
		$this->redirect(array('action'=>'login','backend'=>false));
	}
	/**
	 * Logout
	 */
	public function logout(){
		$this->Auth->logout();
		$this->redirect('/');
	}

	/**
	 * Listar usuarios
	 */
	public function backend_index() {
		$this->set('title_for_layout', 'Listado de usuarios');
		$this->User->recursive = 0;
		//$this->paginate['conditions'] = array('User.enabled'=>1);
		$this->set('users',$this->paginate());
	}

	/**
	 * Agregar un usuario
	 */
	public function backend_add() {
		if ($this->request->is('post')){

			$this->layout='ajax';
			$this->request->data['User']['name'] =  $_POST['name'];
			$this->request->data['User']['email'] =  $_POST['email'];
			$this->request->data['User']['access_level'] = 2;
			$this->request->data['User']['password'] = AuthComponent::password( $_POST['password']);
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$response['response'] ='success';
				$response['message'] ='Se guardaron los datos';
				$this->set('response',$response);
			}else{
				$response['response'] ='error';
				 $message ='No se pudieron guardar los datos  debido a lo siguiente: <br/>';
				foreach($this->User->validationErrors as $key => $error){
					foreach($error as $err){
						$message.=  $err;
					}
				}
				$response['message'] = $message;
				$this->set('response',$response);
			}
		}

	}
	/**
	 * Modificar usuario
	 * @param id int
	 */
	public function backend_edit($id =null) {
			$this->User->id = $id;
			if (!$this->User->exists()) {
				throw new NotFoundException(__('El usuario no existe'));
			}

			if(!empty($this->request->data)){
				if($this->request->data['password_conf']){
					$this->request->data['User']['password'] = AuthComponent::password($this->request->data['password_conf']);
					  unset($this->request->data['password_conf']);
				}
				$this->request->data['User']['name']= $this->request->data['name'];
				$this->request->data['User']['email'] = $this->request->data['email'];

				if ($this->User->save($this->request->data)) {
					$this->Session->setFlash('Usuario modifico con exito','default', array ('class' => 'alert alert-success'));
					$this->redirect(array('action'=>'index'));
				}else{
					$this->Session->setFlash('No se pudo modificar el usuario', 'default', array('class'=>'alert alert-danger'));
				}

			}else{
				$this->set('title_for_layout', 'Modificar usuario');
				$this->request->data = $this->User->read(null,$id);
			}
	}

	/**
	 * Eliminar un usuario
	 * @param id int
	 * */
	public function backend_delete( $id =null) {
		$this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('El usuario no existe'));
        }
		if($this->User->delete($id)){
				$this->Session->setFlash('Usuario eliminando con exito', 'default', array ('class' => 'alert alert-success'));
				$this->redirect($this->referer());
		}
		$this->Session->setFlash('No se pudo eliminar el usuario', 'default',  array('class'=>'alert alert-danger'));

	}
}
?>
