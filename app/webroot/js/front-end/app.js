angular.module('ToDoApp', [])

.controller('TodoController', function($scope, $http) {
    $scope.tasks = [];
    $scope.insert = {};
    $scope.now = new Date().getTime();
    $scope.hideInsert = '';
    $scope.hideUpdate = '';
    $scope.formtitle = "";

    //Mostrar el listado de tareas
    $scope.listTasks = function() {
        $http({
            method: 'GET',
            url: '/my-tasks'
        }).then(function(response) {
            $scope.tasks = response.data;
        }, function(error) {

        });
    }

    //Insertar los datos
    $scope.insertTask = function() {
        var dateFromHTML = $('#datepicker').val();
        $scope.insert.expiration_date = dateFromHTML;
        $http({
            method: 'POST',
            url: '/insert-task',
            data: $scope.insert
        }).then(function(response) {

            if (response.data.response == 'error') {
                swal('No se pudo guardar la tarea', response.data.message, 'error');
            }
            if (response.data.response == 'success') {
                swal('Tarea Agregada', response.data.message, 'success');
                $scope.reset();
                $scope.listTasks();
            }

        }, function(error) {

            swal('Error', 'Por favor corrija los campos del fomulario', 'error');
        });
    }



    //Eliminar una tarea
    $scope.deleteTask = function(id) {
            $http({
                method: 'POST',
                url: '/delete-task',
                data: id
            }).then(function(response) {
                swal('Tarea Eliminada', response.data.message, 'success');
                $scope.listTasks();
            }, function(error) {

            });
        }
        //Modificar tarea
    $scope.editTask = function(id) {
        $http({
            method: 'POST',
            url: '/edit-task',
            data: id
        }).then(function(response) {
            $scope.insert.id = response.data.id;
            $scope.insert.name = response.data.name;
            $scope.insert.description = response.data.description;
            $scope.insert.priority = response.data.priority;
            $('#datepicker').val(response.data.expiration_date);
            $scope.formtitle = "Actualizar Tarea";
            $scope.hideInsert = 'Hide';
            $scope.hideUpdate = 'Show';

        }, function(error) {


        });
    }

    //Actualizar los datos
    $scope.updateTask = function() {

        var dateFromHTML = $('#datepicker').val();
        $scope.insert.expiration_date = dateFromHTML;
        $http({
            method: 'POST',
            url: '/update-task',
            data: $scope.insert
        }).then(function(response) {
            if (response.data.response == 'error') {
                swal('No se pudo guardar la tarea', response.data.message, 'error');
            }
            if (response.data.response == 'success') {
                swal('Tarea Agregada', response.data.message, 'success');
                $scope.reset();
                $scope.listTasks();
            }
        }, function(error) {
            swal('Error', 'Por favor corrija los campos del fomulario', 'error');
        });
    }


    // Funciones auxiliares

    $scope.newTask = function() {
        $scope.formtitle = "Agregar Tarea";
        $scope.hideInsert = 'Show';
        $scope.hideUpdate = 'Hide';
        $scope.reset();
    }

    // Convertir una fecha string en numerica para comparar
    $scope.convertirFecha = function(date) {
        return new Date(date).getTime();
    }

    // Diferencia de dias entre dos fechas
    $scope.daydiff = function(datetwo, dateone) {
            var dayDif = (datetwo - dateone) / 1000 / 60 / 60 / 24;
            return Math.ceil(dayDif);
        }
        // Reestablecer los valores originales
    $scope.reset = function() {
        $scope.insert = {};
        $('#datepicker').val('');
        $('#addTask').hide();
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    }

    //Llamar el listado de tareas
    $scope.listTasks();

});