<?php echo $this->Form->create('User',array('id'=>'frmUsers','url'=>array('action'=>'add'))); ?>
            <div class="form-row">
                <div class="col-md-3 mb-3">

                    <input type="text" class="form-control" id="user-name" placeholder="Nombre"
                    required name="name">
                </div>
                <div class="col-md-3 mb-3">

                    <input type="text" class="form-control" id="user-email" placeholder="Email"  name="email" minlength="4">
				</div>
				<div class="col-md-3 mb-3">
                    <input type="password" class="form-control" id="user-password" placeholder="Clave" name="password" minlength="4">
				</div>
				<div class="col-md-3 mb-3">
                    <input  type="password" class="form-control" id="user-password-conf" placeholder="Confirmar Clave"  name="passConf" minlength="4">
				</div>

			</div>

			<button class="btn btn-success" type="submit" id="saveUser"><i class="fas fa-plus-circle"></i> Guardar</button>
<?php echo $this->Form->end(); ?>
