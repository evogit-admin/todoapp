<!DOCTYPE html>
<html lang="en" ng-app="ToDoApp">
<head>
		<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>
		Mis Tareas
	</title>

		<?php echo $this->Html->script(
			array(
				'front-end/angular.min.js',
				'front-end/app.js',
				'https://cdn.jsdelivr.net/npm/sweetalert2@7.28.11/dist/sweetalert2.all.min.js',
				'https://code.jquery.com/jquery-3.2.1.slim.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js',
				'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js',
				'https://code.jquery.com/jquery-1.12.4.js',
				'https://code.jquery.com/ui/1.12.1/jquery-ui.js',
				'front-end-form'


				));
		?>

		<?php echo $this->Html->css(
			array(
			'bootstrap.min.css',
			'front-end-styles',
			'https://use.fontawesome.com/releases/v5.5.0/css/all.css',
			'https://fonts.googleapis.com/css?family=Quicksand',
			'http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css'

		));
		 ?>

</head>
<body ng-controller="TodoController">
<div class="container">
<div class="jumbotron">
<div class="container-heading">
	<h2 class="display-5"> Mi listado de tareas</h2>
	<hr class="my-4">
	<p class="lead">Bienvenido  <?php
	$user = $this->Session->read('Auth.User');
	echo $user['name'];
	echo $this->Html->link('<i class="fas fa-sign-out-alt"></i>Salir', array('controller'=>'users','action'=>'logout'),array('class'=>'btn btn-primary float-right','escape'=>false)); ?>
	</p>

  </div>
</div>
<?php echo $content_for_layout; ?>
</div>

</body>
</html>
