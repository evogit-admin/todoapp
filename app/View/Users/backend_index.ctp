<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">

	<div class="btn-toolbar mb-2 mb-md-0">
		<button type="button" class="btn btn-primary" id='new-user'>
			<i class="fas fa-user-plus"></i> Crear  Usuario
		</button>

	</div>

    </div>

	<div class="row" id='users-form'>
		<?php echo $this->element('Backend/users-form'); ?>
	</div>

	<h2>Listado de usuarios</h2>
	<div class="table-responsive">
	<?php echo $this->element('Backend/users-table',array('users'=>$users)); ?>

</div>
