<div class="col-md-6">
<h2>Modificar usuario</h2>
<?php echo $this->Form->create('User',array('id'=>'frmUsers'));?>
  <div class="form-group">
    <label for="Name">Nombre</label>
	<?php echo $this->Form->input('id'); ?>
	<?php echo $this->Form->input('name',array('class'=>'form-control','name'=>'name','label'=>false)) ?>

  </div>
  <div class="form-group">
    <label for="Email">Email</label>
	<?php echo $this->Form->input('email',array('class'=>'form-control','name'=>'email','label'=>false)) ?>
  </div>

    <div class="form-group">
    <label for="Clave">Cambiar Clave</label>
	<?php echo $this->Form->input('password_conf',array('class'=>'form-control','name'=>'password_conf','type'=>'password','label'=>false)) ?>
  </div>


  <button type="submit" class="btn btn-success">Modificar</button>
<?php echo $this->Form->end(); ?>
</div>
