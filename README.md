# TODO APP
### Desarrollada por: Ing. Luis Gabriel Rodríguez <luisgabriel84@gmail.com>

## Requerimientos desarrollados
### Backend
- [X] Creación de usuarios con nombre, email y contraseña
- [x] Modificar usuarios
- [x] Eliminar usuarios
- [x]  Autenticación para el admin para usuario y contraseña 

### Frontend
- [x] Autenticación por usuario y contraseña
- [x] Interfáz para editar /Eliminar tareas
- [x] Modal para agregar nuevas tareas
- [x] Mostrar alertas de tareas por vencer
- [x] Mostrar solo las tareas del usuario que las creo

## Tecnologías utilizadas
- Cakephp 2.0
- Boostrap 4.0
- Jquery 
- Jquery Ui ( datepicker)
- AngularJS 1.7
- Mysql 

## Instrucciones de instalación

### Ambiente de desarrollo
- Apache Server 2  ( con mod_rewrite habiltado)
- PHP 5.6+
- MySql 5

1. Habilitar reescritura de urls del directorio raíz del Apache (importante)
```
<Directory /var/www/>
     Options FollowSymLinks
     AllowOverride All
     Require all granted
</Directory>
```

2. Crear la base de datos en MySql con el archivo SQL adjunto en la carpeta git
3. clonar el repositorio raíz a la carpeta de su elección
4. chmod 777 su_directorio -R
5. Acceda a la carpeta su_directorio/app/Config/database.php
6. Modifique los parametros de conexión para reflejar los de su ambiente
```php
	public $default = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => '1234',
		'database' => 'todo-app',
		'prefix' => '',
		//'encoding' => 'utf8',
	);
  ```
  
  7. Si el proyecto quedo propiamente configurado la primera ventana debe ser la ventana de autenticación, el archivo SQL viene con un usuario de tipo administrador para proceder a crear los usuarios regulares:
  

      Usuario: admin@gmail.com
      Clave: 123456
